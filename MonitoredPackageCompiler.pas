{**
 DelphiPI (Delphi Package Installer)
 Author  : ibrahim dursun (ibrahimdursun gmail)
 License : GNU General Public License 2.0
**}
unit MonitoredPackageCompiler;

interface
uses progressmonitor, packagecompiler, packageinfo, CompilationData;

type
  TPackageCompileEvent = procedure(const package: TPackageInfo; status: TPackageStatus) of object;
  TMonitoredPackageCompiler = class(TPackageCompiler)
  private
    fMonitor: IProgressMonitor;
  protected
    procedure RaiseEvent(const packageInfo: TPackageInfo; status: TPackageStatus);virtual;
    procedure PrepareExtraOptions; override;
    
  public
    constructor Create(const compilationData: TCompilationData; const aMonitor: IProgressMonitor); reintroduce;
    procedure ResolveSourcePaths; override;
    procedure CompilerOutputCallback(const line:string); virtual;
    function CompilePackage(const packageInfo: TPackageInfo): boolean; override;
    function InstallPackage(const packageInfo: TPackageInfo): boolean; override;
    procedure Compile; override;
  end;

implementation

uses Classes, System.SysUtils, System.StrUtils, JclIDEUtils, JclCompilerUtils;

constructor TMonitoredPackageCompiler.Create(const compilationData:TCompilationData; const aMonitor: IProgressMonitor);
begin
  Assert(aMonitor <> nil,'monitor cannot be null'); { TODO -oidursun -c : a default null implementation can be set }
  inherited Create(compilationData);
  fMonitor := aMonitor;
end;

procedure TMonitoredPackageCompiler.Compile;

  procedure SetupActivePlatform(APlatForm: TJclBDSPlatform);
  begin
    ACTIVE_PLATFORM := APlatForm;
    try
      if (ACTIVE_PLATFORM = bpWin64) then
      begin
        if (RightStr(CompilationData.DCUOutputFolder, 2) = '32') then
        begin
          CompilationData.DCUOutputFolder :=
            Copy(CompilationData.DCUOutputFolder, 1, Length(CompilationData.DCUOutputFolder) - 2) + '64';
        end
        else
        begin
          CompilationData.DCUOutputFolder :=
            IncludeTrailingPathDelimiter(CompilationData.DCUOutputFolder) + BDSPlatformWin64;
        end;
      end;
    finally
      Installation.AddToLibrarySearchPath(CompilationData.DCUOutputFolder, ACTIVE_PLATFORM);
      CompilationData.BPLOutputFolder := Installation.BPLOutputPath[ACTIVE_PLATFORM];
      CompilationData.DCPOutputFolder := Installation.DCPOutputPath[ACTIVE_PLATFORM];
    end;
  end;

var
  compiler: TJclDCC32;
  dcuOutputFolder: string;

begin
  SetupActivePlatform(bpWin32);
  compiler := Installation.DCC32;
  compiler.OutputCallback := CompilerOutputCallback;

  fMonitor.Log('!!');
  fMonitor.Log('!!Building packages for ' + Installation.Description);
  fMonitor.Log('!!');

  //Installation.DCC32.OutputCallback := CompilerOutputCallback;

  fMonitor.Started;
  fMonitor.Log('-=Started');

  inherited;

  if not(Cancel) and (clDcc64 in Installation.CommandLineTools) then
  begin
    {
      Backup to restore win32 starting point.
      This prevents corruption of folder structure and search paths when user
      executes compile multiple times.
    }
    dcuOutputFolder := CompilationData.DCUOutputFolder;
    try
      SetupActivePlatform(bpWin64);
      compiler := (Installation as TJclBDSInstallation).DCC64;
      compiler.OutputCallback := CompilerOutputCallback;

      ForceDirectories(CompilationData.BPLOutputFolder);
      ForceDirectories(CompilationData.DCPOutputFolder);

      fMonitor.Started;
      fMonitor.Log('-=Compile 64bit packages');

      inherited;

    finally
      CompilationData.DCUOutputFolder := dcuOutputFolder;
      SetupActivePlatform(bpWin32); { Restore before saving }
    end;
  end;

  fMonitor.Finished;
  fMonitor.Log('-=Finished');
end;

function TMonitoredPackageCompiler.CompilePackage(
  const packageInfo: TPackageInfo): boolean;
begin
  fMonitor.Log('-=Compiling: ' + packageInfo.PackageName);
  fMonitor.Log('Required :'+packageInfo.RequiredPackageList.DelimitedText);
  fMonitor.Log('Contains :'+packageInfo.ContainedFileList.DelimitedText);

  RaiseEvent(packageInfo, psCompiling);
  Result := inherited CompilePackage(packageInfo);
  if not Result then
    RaiseEvent(packageInfo, psError)
  else
    RaiseEvent(packageInfo, psSuccess);  
  fMonitor.Log('Finished');
end;

procedure TMonitoredPackageCompiler.CompilerOutputCallback(const line: string);
begin
  fMonitor.CompilerOutput(line);
end;

function TMonitoredPackageCompiler.InstallPackage(
  const packageInfo: TPackageInfo): boolean;
begin
   Result := inherited InstallPackage(packageInfo);
end;

procedure TMonitoredPackageCompiler.PrepareExtraOptions;
var
  line: string;
begin
  inherited;
  fMonitor.Log('-=All Path:');
  for line in AllPaths do
    fMonitor.Log(line);

  fMonitor.Log('-=Compiler Options:');
  fMonitor.Log(fExtraOptions);
end;

procedure TMonitoredPackageCompiler.RaiseEvent(const packageInfo: TPackageInfo;
  status: TPackageStatus);
begin
  fMonitor.PackageProcessed(packageInfo, status);
  if (status = psSuccess) or (status = psError) then
    packageInfo.Status := status;
end;

procedure TMonitoredPackageCompiler.ResolveSourcePaths;
var
  path: string;
begin
  inherited;
  fMonitor.Log('-=Source Paths:');
  for path in SourceFilePaths do
    fMonitor.Log(path);
end;

end.
