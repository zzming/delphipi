{**
 DelphiPI (Delphi Package Installer)
 Author  : ronald siekman
 License : GNU General Public License 2.0
**}
unit PageFolderOptions;

interface

uses
  Classes, CompilationData, Windows, Messages, SysUtils, Variants, Graphics,
  Vcl.Controls, Forms, Dialogs, PageBase, StdCtrls, ExtCtrls, WizardIntfs,
  Vcl.ComCtrls, Vcl.Buttons, System.Actions, Vcl.ActnList, System.ImageList,
  Vcl.ImgList, Vcl.Menus, Generics.Collections;

type
  TUpdate = (add, replace, delete);

  TSelectFolderOptions = class(TWizardPage)
    grpLibrarySearchPaths: TGroupBox;
    listSearchPaths: TListBox;
    grpBrowsingPaths: TGroupBox;
    listBrowsingPaths: TListBox;
    labelTemporary: TLabel;
    Panel1: TPanel;
    Panel2: TPanel;
    editSearchPath: TEdit;
    buttonSearchFolder: TSpeedButton;
    Panel3: TPanel;
    buttonBrowsingFolder: TSpeedButton;
    editBrowsingPath: TEdit;
    buttonReplaceSearch: TButton;
    buttonAddSearch: TButton;
    buttonDeleteSearch: TButton;
    Panel4: TPanel;
    buttonReplaceBrowse: TButton;
    buttonAddBrowse: TButton;
    buttonDeleteBrowse: TButton;
    ImageList: TImageList;
    ActionList: TActionList;
    actSelectFolderSearchPath: TAction;
    PopupMenu: TPopupMenu;
    actTemporary: TAction;
    emporary1: TMenuItem;
    actSelectFolderBrowsingPath: TAction;
    actAddFolderSearchPath: TAction;
    actReplaceFolderSearchPath: TAction;
    actDeleteFolderSearchPath: TAction;
    actAddFolderBrowsingPath: TAction;
    actReplaceFolderBrowsingPath: TAction;
    actDeleteFolderBrowsingPath: TAction;
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure listSearchPathsDrawItem(Control: TWinControl; Index: Integer;
      Rect: TRect; State: TOwnerDrawState);
    procedure actSelectFolderSearchPathExecute(Sender: TObject);
    procedure actTemporaryExecute(Sender: TObject);
    procedure actSelectFolderBrowsingPathExecute(Sender: TObject);
    procedure actAddFolderSearchPathExecute(Sender: TObject);
    procedure actReplaceFolderSearchPathExecute(Sender: TObject);
    procedure actDeleteFolderSearchPathExecute(Sender: TObject);
    procedure actAddFolderBrowsingPathExecute(Sender: TObject);
    procedure actReplaceFolderBrowsingPathExecute(Sender: TObject);
    procedure actDeleteFolderBrowsingPathExecute(Sender: TObject);
    procedure listSearchPathsClick(Sender: TObject);
    procedure listBrowsingPathsClick(Sender: TObject);
    procedure PopupMenuPopup(Sender: TObject);
    procedure editSearchPathChange(Sender: TObject);
    procedure editBrowsingPathChange(Sender: TObject);
  private
    procedure ShowCompilationDataSearchPaths;
    procedure ShowCompilationDataBrowsingPaths;
    procedure UpdateFolder(paths: TList<TFolder>; list: TListBox; state: TUpdate);
  public
    constructor Create(Owner: TComponent;
      const compilationData: TCompilationData); override;
    procedure UpdateWizardState; override;
    function CanShowPage: Boolean; override;
  end;

var
  SelectFolderOptions: TSelectFolderOptions;

implementation

uses gnugettext, Utils;

{$R *.dfm}

{ TSelectDelphiInstallationPage }

constructor TSelectFolderOptions.Create(Owner: TComponent;
  const compilationData: TCompilationData);
begin
  inherited;

  FCompilationData := compilationData;
end;

procedure TSelectFolderOptions.editBrowsingPathChange(Sender: TObject);
var
  folder: TFolder;

begin
  buttonAddBrowse.Enabled := False;
  buttonReplaceBrowse.Enabled := False;
  buttonDeleteBrowse.Enabled := False;

  if (listBrowsingPaths.ItemIndex >= 0) then begin
    folder := TFolder(listBrowsingPaths.Items.Objects[listBrowsingPaths.ItemIndex]);
    buttonAddBrowse.Enabled := folder.Name <> editBrowsingPath.Text;
    buttonReplaceBrowse.Enabled := buttonAddBrowse.Enabled;
    buttonDeleteBrowse.Enabled := not(buttonAddBrowse.Enabled);
  end;
end;

procedure TSelectFolderOptions.editSearchPathChange(Sender: TObject);
var
  folder: TFolder;

begin
  buttonAddSearch.Enabled := False;
  buttonReplaceSearch.Enabled := False;
  buttonDeleteSearch.Enabled := False;

  if (listSearchPaths.ItemIndex >= 0) then begin
    folder := TFolder(listSearchPaths.Items.Objects[listSearchPaths.ItemIndex]);
    buttonAddSearch.Enabled := folder.Name <> editSearchPath.Text;
    buttonReplaceSearch.Enabled := buttonAddSearch.Enabled;
    buttonDeleteSearch.Enabled := not(buttonAddSearch.Enabled);
  end;
end;

procedure TSelectFolderOptions.FormCreate(Sender: TObject);
begin
  inherited;

  TranslateComponent(self);
  ShowCompilationDataSearchPaths;
  ShowCompilationDataBrowsingPaths;
end;

procedure TSelectFolderOptions.FormResize(Sender: TObject);
begin
  inherited;

  grpLibrarySearchPaths.Height := Height div 2;
end;

procedure TSelectFolderOptions.listBrowsingPathsClick(Sender: TObject);
begin
  if (listBrowsingPaths.ItemIndex >= 0) then
    editBrowsingPath.Text := listBrowsingPaths.Items[listBrowsingPaths.ItemIndex];
end;

procedure TSelectFolderOptions.listSearchPathsClick(Sender: TObject);
begin
  if (listSearchPaths.ItemIndex >= 0) then
    editSearchPath.Text := listSearchPaths.Items[listSearchPaths.ItemIndex];
end;

procedure TSelectFolderOptions.listSearchPathsDrawItem(Control: TWinControl;
  Index: Integer; Rect: TRect; State: TOwnerDrawState);
begin
  with (Control as TListBox).Canvas do
  begin
    FillRect(Rect);
    if TFolder((Control as TListBox).Items.Objects[Index]).Temporary then
      if (odSelected in State) then begin
        Brush.Color := clGrayText;
        FillRect(Rect);
      end
      else Font.Color := clGrayText;

    TextOut(Rect.Left, Rect.Top, (Control as TListBox).Items[Index]);
  end;
end;

procedure TSelectFolderOptions.PopupMenuPopup(Sender: TObject);
var
  folder: TFolder;

begin
  if (listSearchPaths.ItemIndex >= 0) then begin
    folder := TFolder(listSearchPaths.Items.Objects[listSearchPaths.ItemIndex]);
    actTemporary.Checked := folder.Temporary;
  end;
end;

procedure TSelectFolderOptions.actAddFolderBrowsingPathExecute(Sender: TObject);
begin
  UpdateFolder(fCompilationData.BrowsingPaths, listBrowsingPaths, TUpdate.add);
end;

procedure TSelectFolderOptions.actAddFolderSearchPathExecute(Sender: TObject);
begin
  UpdateFolder(fCompilationData.SearchPaths, listSearchPaths, TUpdate.add);
end;

procedure TSelectFolderOptions.actDeleteFolderBrowsingPathExecute(
  Sender: TObject);
begin
  UpdateFolder(fCompilationData.BrowsingPaths, listBrowsingPaths, TUpdate.delete);
end;

procedure TSelectFolderOptions.actDeleteFolderSearchPathExecute(
  Sender: TObject);
begin
  UpdateFolder(fCompilationData.SearchPaths, listSearchPaths, TUpdate.delete);
end;

procedure TSelectFolderOptions.actReplaceFolderBrowsingPathExecute(
  Sender: TObject);
begin
  UpdateFolder(fCompilationData.BrowsingPaths, listBrowsingPaths, TUpdate.replace);
end;

procedure TSelectFolderOptions.actReplaceFolderSearchPathExecute(
  Sender: TObject);
begin
  UpdateFolder(fCompilationData.SearchPaths, listSearchPaths, TUpdate.replace);
end;

procedure TSelectFolderOptions.actSelectFolderBrowsingPathExecute(
  Sender: TObject);
begin
  SelectFolder(editBrowsingPath);
end;

procedure TSelectFolderOptions.actSelectFolderSearchPathExecute(Sender: TObject);
begin
  SelectFolder(editSearchPath);
end;

procedure TSelectFolderOptions.actTemporaryExecute(Sender: TObject);
var
  folder: TFolder;

begin
  actTemporary.Checked := not(actTemporary.Checked);
  if (listSearchPaths.ItemIndex >= 0) then begin
    folder := TFolder(listSearchPaths.Items.Objects[listSearchPaths.ItemIndex]);
    folder.Temporary := actTemporary.Checked;
    listSearchPaths.Invalidate;
  end;
end;

function TSelectFolderOptions.CanShowPage: Boolean;
begin
   Result := True;
end;

procedure TSelectFolderOptions.ShowCompilationDataBrowsingPaths;
var
  folder: TFolder;

begin
  for folder in fCompilationData.BrowsingPaths do
    listBrowsingPaths.AddItem(folder.Name, folder);

  if (listBrowsingPaths.Count > 0) then begin
    listBrowsingPaths.ItemIndex := 0;
    editBrowsingPath.Text := listBrowsingPaths.Items[0];
  end;
end;

procedure TSelectFolderOptions.ShowCompilationDataSearchPaths;
var
  folder: TFolder;

begin
  for folder in fCompilationData.SearchPaths do
    listSearchPaths.AddItem(folder.Name, folder);

  if (listSearchPaths.Count > 0) then begin
    listSearchPaths.ItemIndex := 0;
    editSearchPath.Text := listSearchPaths.Items[0];
  end;
end;

procedure TSelectFolderOptions.UpdateFolder(paths: TList<TFolder>;
  list: TListBox; state: TUpdate);
var
  folder: TFolder;
  folderName: string;
  index: Integer;

begin
  if (list = listSearchPaths) then folderName := editSearchPath.Text
  else folderName := editBrowsingPath.Text;

  case state of
    Add:
      begin
        folder := TFolder.Create(folderName);
        paths.Add(folder);
        list.AddItem(folder.Name, folder);
      end;
    Replace:
      begin
        if (list.ItemIndex >= 0) then begin
          folder := TFolder(list.Items.Objects[list.ItemIndex]);
          folder.Name := folderName;
          list.Items[list.ItemIndex] := folder.Name;
        end;
      end;
    Delete:
      begin
        if (list.ItemIndex >= 0) then begin
          folder := TFolder(list.Items.Objects[list.ItemIndex]);
          list.Items.Delete(list.ItemIndex);
          if (list = listSearchPaths) then editSearchPath.Text := ''
          else editBrowsingPath.Text := '';
          index := paths.IndexOf(folder);
          if (index <> -1) then
          begin
            folder.Free;
            paths.Delete(index);
          end;
        end;
      end;
  end;

  if (list = listSearchPaths) then editSearchPathChange(Self)
  else editBrowsingPathChange(Self);
end;

procedure TSelectFolderOptions.UpdateWizardState;
begin
  inherited;
  wizard.SetHeader(_('Add optional Search and Browsing Paths'));
  wizard.SetDescription(_('Please add Search Paths that will affect the compilation'));
end;

end.
